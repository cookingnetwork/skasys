package com.spiceup.accountservice;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RegisterPayload {
    private String name;
    private String firstName;
    private String email;
    private String password;
}
