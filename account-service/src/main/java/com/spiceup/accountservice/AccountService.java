package com.spiceup.accountservice;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {
    private AccountRepository accountRepository;
    private AccountProperties accountProperties;
    private WebClient.Builder webClientBuilder;
    private JavaMailSender emailSender;


    @Autowired
    public AccountService(AccountRepository accountRepository, AccountProperties accountProperties, WebClient.Builder webClientBuilder, JavaMailSender emailSender) {
        this.accountRepository = accountRepository;
        this.accountProperties = accountProperties;
        this.webClientBuilder = webClientBuilder;
        this.emailSender = emailSender;
    }

    public String registerAccount(RegisterPayload payload) throws JSONException {
        //validate email is not used yet
        if(accountRepository.findById(payload.getEmail()).isPresent()){
            return new JSONObject().put("request","failed").toString();
        }
        //create user in user-service and get UserID!
        String json = new JSONObject()
                .put("name", payload.getName())
                .put("firstName", payload.getFirstName())
                .toString();

        String uri = "http://"+accountProperties.getUserServiceServer()+":"+accountProperties.getUserServicePort()+"/users/create";
        Long userId = webClientBuilder.build()
                .post()
                .uri(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(json)
                .retrieve()
                .bodyToMono(Long.class)
                .block();

        //create Account in account database with payload and userID
        Account account = new Account();
        account.setEmail(payload.getEmail());
        account.setPassword(payload.getPassword());
        account.setUserId(userId);
        accountRepository.save(account);

        return new JSONObject()
                .put("request","success")
                .put("userID",userId)
                .toString();
    }

    public String loginWithAccount(LoginPayload payload) throws JSONException {
        //validate credentials
        Optional<Account> account = accountRepository.findById(payload.getEmail());
        if(account.isEmpty()){
            return new JSONObject()
                    .put("loggedIn",false)
                    .put("userID",null)
                    .toString();
        }
        if(account.get().getPassword().equals(payload.getPassword())){
            return new JSONObject()
                    .put("loggedIn",true)
                    .put("userID",account.get().getUserId())
                    .toString();
        }
        return new JSONObject()
                .put("loggedIn",false)
                .put("userID",null)
                .toString();
    }

    //DEBUG
    public List<Account> getAllAcccounts(){
        List<Account> accounts = new ArrayList<>();
        accountRepository.findAll().forEach(accounts::add);
        return accounts;
    }

    public String forgotPassword(String userEmail) {
        //validate if email exists
        Optional<Account> accountOptional = accountRepository.findById(userEmail);
        if(accountOptional.isEmpty()){
            return "User with email "+userEmail+" does not exist.";
        }

        Account account = accountOptional.get();
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("spiceup.cookingnetwork@gmail.com");
        message.setTo(userEmail);
        message.setSubject("Your password to the SpiceUp Cooking Network.");
        String builder = "You received this email because you used the \"Forgot Password\" option.\nThe password for the account " +
                userEmail +
                " is: " +
                account.getPassword() +
                " .\n\n" +
                "Thank you for using our network!\nThe SpiceUp Team";
        message.setText(builder);
        emailSender.send(message);
        return "Password was sent to "+userEmail;
    }


}
