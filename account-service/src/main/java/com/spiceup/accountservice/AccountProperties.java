package com.spiceup.accountservice;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "spiceup")
@Data
public class AccountProperties {
    private String userServiceServer;
    private String userServicePort;
}
