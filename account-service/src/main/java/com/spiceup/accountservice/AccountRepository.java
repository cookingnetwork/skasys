package com.spiceup.accountservice;


import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account,String> {

}