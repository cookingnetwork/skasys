package com.spiceup.accountservice;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="account")
public class Account {

    @Id
    private String email;
    private String password;
    private Long userId;
}
