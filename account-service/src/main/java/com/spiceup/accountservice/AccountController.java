package com.spiceup.accountservice;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/account")
public class AccountController {

    private AccountService accountService;

    @Autowired
    public AccountController (AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/register")
    public String registerAccount(@RequestBody RegisterPayload payload) throws JSONException {
        return accountService.registerAccount(payload);
    }

    @PostMapping("/login")
    public String loginWithAccount(@RequestBody LoginPayload payload) throws JSONException {
        return accountService.loginWithAccount(payload);
    }


    @PostMapping("/forgot")
    public String forgotPassword(@RequestParam("email") String userEmail){
        return accountService.forgotPassword(userEmail);
    }

    @GetMapping
    public List<Account> getAllAccounts(){
        return accountService.getAllAcccounts();
    }

}
