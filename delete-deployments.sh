#!/bin/bash
echo "Deleting all deployments to cluster..."
kubectl delete -f post-service/deployment.yaml
kubectl delete -f post-service/deployment-database.yaml
kubectl delete -f post-service/deployment-pv.yaml

kubectl delete -f user-service/deployment.yaml
kubectl delete -f user-service/deployment-database.yaml
kubectl delete -f user-service/deployment-pv.yaml

kubectl delete -f feed-service/deployment.yaml

kubectl delete -f api-gateway/deployment.yaml

kubectl delete -f account-service/deployment.yaml
kubectl delete -f account-service/deployment-database.yaml
kubectl delete -f account-service/deployment-pv.yaml

kubectl delete -f meeting-service/deployment.yaml
kubectl delete -f meeting-service/deployment-database.yaml
kubectl delete -f meeting-service/deployment-pv.yaml