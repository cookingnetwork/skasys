import "react-native-gesture-handler";
import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import NavMenue from "./app/components/NavMenue";

export default function App() {
  return (
    <NavigationContainer>
      <NavMenue isLoggedIn = {true} />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

//NEEDED first
// npm install @react-navigation/native
// expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view
// make sure: import 'react-native-gesture-handler';    is at the TOP
// npm install @react-navigation/bottom-tabs
// npm install expo-linear-gradient
// expo install expo-image-picker
// npm install --save styled-components
