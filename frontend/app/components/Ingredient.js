import React from "react";
import {View, Button, TouchableWithoutFeedback} from "react-native";
import StyledText from "./StyledText";
import {colors} from "../config/styles";

function Ingredient({ item, deleteIngredientHandler }) {
  return (
    <View key={item.key}>
      <StyledText dark>{item.name}</StyledText>
      <Button
        title="Delete"
        color={colors.lightBg}
        onPress={() => deleteIngredientHandler(item.key)}
      />
    </View>
  );
}

export default Ingredient;
