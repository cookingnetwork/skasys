import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import styled from "styled-components/native";

const StyledText = styled.Text`
  color: ${(props) => (props.dark ? "#000" : "white")};
  font-family: Helvetica;

  ${({ title, large, small }) => {
    switch (true) {
      case title:
        return `font-size: 32px`;
      case large:
        return `font-size: 20px`;
      case small:
        return `font-size: 13px`;
    }
  }}

  ${({ bold, heavy }) => {
    switch (true) {
      case bold:
        return `font-weight: 600`;
      case heavy:
        return `font-weight: 700`;
    }
  }}
`;

export default StyledText;
