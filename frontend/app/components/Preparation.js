import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  SafeAreaView,
  Platform,
  StatusBar,
  ImageBackground,
  FlatList,
  ScrollView,
} from "react-native";
import styled from "styled-components/native";
import { AntDesign } from "@expo/vector-icons";

import StyledText from "./StyledText";
import colors from "../config/colors";

function Preparation({ data }) {
  const ing = data.ingredients;
  return (
    <PreparationContainer>
      <Ingredients>
        <StyledText large light heavy>
          Zutaten:
        </StyledText>
        <View style={{ flexDirection: "column" }}>
          {ing.map((item) => {
            return (
              <View key={Math.random().toString()}>
                <StyledText light>{item + "\n"}</StyledText>
              </View>
            );
          })}
        </View>
      </Ingredients>
      <SeparationLine></SeparationLine>
      <PreparationView>

        <StyledText large light heavy>
          Zubereitung:
        </StyledText>
        <StyledText light>{data.description}</StyledText>
      </PreparationView>
    </PreparationContainer>
  );
}

export default Preparation;

const PreparationContainer = styled.View`
  flex-direction: column;
  margin-top: 20px;
  margin-bottom: 20px;
  margin-left: 100px;
  margin-right:100px;
`;

const Ingredients = styled.View`
  margin-bottom: 50px;
`;

const PreparationView = styled.View`
  flex: 1;
  margin-top: 10px;
`;

const Title = styled.View`
  font-size: 1.5em;
  color: colors.white;
`;

const SeparationLine = styled.View`
  border-bottom-color: ${colors.green};
  border-bottom-width: 2px;
`;
