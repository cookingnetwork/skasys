import React from "react";
import {StyleSheet, Text, View, Image, ImageBackground, SafeAreaView, StatusBar} from "react-native";
import styled from "styled-components/native";
import { AntDesign } from "@expo/vector-icons";

import StyledText from "./StyledText";
import {colors, gs} from "../config/styles";
import {LinearGradient} from "expo-linear-gradient";

function TopBar({data}) {
  return (
      <SafeAreaView style={styles.container}>
      <LinearGradient colors={[colors.green, colors.red]} start={[0, 3]} end={[1, 2]} >
          <View style={{ marginHorizontal: 32, paddingVertical: 24 }}>

              <View style={styles.imageContainer}>
                  <View style={[gs.center, { marginVertical: 5 }]}>

                      <StyledText large light heavy>
                          {data.name}
                      </StyledText>
                  </View>
          </View>
          </View>
      </LinearGradient>
      </SafeAreaView>
  );
}

export default TopBar;

const styles = StyleSheet.create({
    imageContainer: {
        // ...gs.center, dunno why is not wokring like this, should do the same like next two lines
        alignItems: "center",
        justifyContent: "center",
        marginTop: 5,
    },
text:{
    fontSize: 32,
    color: colors.white
}, container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },item: {
        padding: 20,
        marginVertical: 10,
        marginHorizontal: 120,
        alignItems: 'center',
    }})

