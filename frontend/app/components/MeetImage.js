import React from "react";
import styled from "styled-components/native";
import {View, ImageBackground, Button, TouchableOpacity, Text} from "react-native";
import { AntDesign, HeartOutlined } from "@expo/vector-icons";

import StyledText from "./StyledText";
import{ colors} from "../config/styles";

function PostImage({ data, navigation, user }) {
    return (
        <FoodPicture
            source={"http://34.141.19.16:8083/posts/" + data.postId + "/image"}
        >
            <HeartView>
                <TouchableOpacity onPress={() => navigation.navigate("Post", { id: data.postId })} style={styles.loginBtn}>
                    <Text style={{color:colors.white}}>Post ansehen</Text>
                </TouchableOpacity>
            </HeartView>
            { !(data.userId === user)?
                (<PlusView>
                <TouchableOpacity onPress={() => navigation.navigate("Post", { id: data.id })} style={styles.btn}>
                    <Text style={{color:colors.white}}>Teilnehmen</Text>
                </TouchableOpacity>
            </PlusView>) : (<></>) }
        </FoodPicture>
    );
}

export default PostImage;

const styles = {
    loginBtn: {
        width: "30%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        backgroundColor: colors.darkBg,
    },
    btn: {
        width: "10%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        backgroundColor: colors.darkBg,
    }
}

const FoodPicture = styled.ImageBackground`
  width: 100%;
  height: 350px;
`;

const RecipeTitle = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  flex: 1;
  padding: 10px;
`;

const HeartView = styled.View`
  justify-content: flex-start;
  align-items: flex-end;
  margin: 10px;
`;

const PlusView = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  flex: 1;
  padding: 10px;
`;