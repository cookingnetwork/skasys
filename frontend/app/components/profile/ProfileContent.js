import React, { Component } from 'react'
import { Text, View, StyleSheet, Image,TouchableOpacity, } from 'react-native'
import { color } from 'react-native-reanimated'
import { gs, colors } from '../../config/styles'
import { useNavigation } from '@react-navigation/native';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import LoginScreen from "../../screens/LoginScreen";

const photos = [
  require('../../assets/brunch.jpg'),
]

const Tab = createBottomTabNavigator();

export default class ProfileContent extends Component {
  render() {
    var posts = this.props.posts
    var meets = this.props.meets


    return (
        <View style={[gs.sectionContainer, {marginTop: 8}, {marginBottom: 150}]}>
          <Text style={gs.sectionTitle}> My Posts </Text>
          <View style={styles.photosContainer}>
            {posts.map((post, index) => {
              var image = "http://34.141.19.16:8083/posts/" + post.id + "/image"
              return (
                  <TouchableOpacity key={index} onPress={() => this.props.navigation.navigate("Post", {id: post.id})}>
                    <Image
                        source={image}
                        style={[styles.photo,
                          {marginRight: (index + 1) % 9 === 0 ? 0 : 12,}
                        ]}>
                    </Image>
                  </TouchableOpacity>
              )
            })}
          </View>

            <Text style={gs.sectionTitle}> My Meets </Text>
            <View style={styles.photosContainer}>
                {meets.map((meet, index) => {
                    var image = "http://34.141.19.16:8083/posts/" + meet.postId + "/image"
                    return (
                        <TouchableOpacity key={index} onPress={() => this.props.navigation.navigate("Meet", {id: meet.id})}>
                          <View style={[styles.photosContainer,
                            {marginRight: (index + 1) % 9 === 0 ? 0 : 12,}
                          , {backgroundColor:colors.lightBg}]}>
                            <View ><Image
                                source={image}
                                style={[styles.photo
                                ]}>

                            </Image><Text style={styles.text}>{meet.name} {meet.date}</Text></View>
                          </View>
                </TouchableOpacity>
                    )
                })}
            </View>
        </View>)
  }
}


const styles = StyleSheet.create({
  photosContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
    borderRadius: 8,
    padding:20,
    alignItems: 'center',
    alignContent: 'center'
  },
  photo: {
    width: 150,
    height: 150,
    marginBottom: 12,
    borderRadius: 8,
  },
  text: {
    color:colors.white,
    fontsize: 20,
  }
})







// import React from 'react'
// import {
//   Text,
//   View,
//   FlatList,
//   Animated
// } from 'react-native'

// const DATA = [
//   {
//       id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
//       title: 'Scallion Pancakes',
//       owner: 'Michael J.',
//       uri: require('../../assets/scallion.jpg')
//   },
//   {
//       id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
//       title: 'Erdbeerkuchen',
//       owner: 'Susanne H.',
//       uri: require('../../assets/erdbeer.jpg')
//   },
//   {
//       id: '58694a0f-3da1-471f-bd96-145571e29d72',
//       title: 'Kokosnuss Hähnchen',
//       owner: 'LeckaSchmecka51',
//       uri: require('../../assets/curry.jpg')
//   },
//   {
//       id: '58694a0f-3da1-471f-bd96-145571e29d62',
//       title: 'Brunch Ideen',
//       owner: 'RobertKocht',
//       uri: require('../../assets/brunch.jpg')
//   },
// ];

// const ProfileContent = () => {

//   return (
//    true
//   )
// }

// export default ProfileContent