import React, {useState, useEffect} from 'react'
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image
} from 'react-native'
import { Ionicons, Entypo } from '@expo/vector-icons'
import { LinearGradient } from 'expo-linear-gradient' //requires npm install expo-linear-gradient first
import { gs, colors } from "../../config/styles.js"
import {Buffer} from "buffer";


export default class ProfileHeader extends React.Component {
  render() {
    const { user } = this.props;
    const isLoggedInUser = user.id === this.props.loggedInUser
    const name = `${user.firstName} ${user.name}`; //wird über Profile Screen gecalled


    return (
        <LinearGradient colors={[colors.green, colors.red]} start={[0, 3]} end={[1, 2]} >
          <View style={{ marginHorizontal: 32, paddingVertical: 64 }}>

            <View style={styles.imageContainer}>
              <View>
                {/* <View style={styles.check}>
=======
      <LinearGradient
        colors={[colors.green, colors.red]}
        start={[0, 3]}
        end={[1, 2]}
      >
        <View style={{ marginHorizontal: 32, paddingVertical: 64 }}>
          <View style={styles.imageContainer}>
            <View>
              <View style={styles.check}>
>>>>>>> origin/CN-31
                <Ionicons name="md-checkmark" size={20} color={colors.red} />
              </View> */}
                <Image
                    source={{ uri: require("../../assets/stockface.jpeg") }}
                    style={{ width: 150, height: 150, borderRadius: 100 }}
                />
              </View>
            </View>

            <View style={[gs.center, { marginVertical: 12 }]}>
              <Text style={gs.title}> {name} </Text>
              <Text style={[gs.subTitle, { marginTop: 8 }]}> Food Lover</Text>

              {!isLoggedInUser ? (<TouchableOpacity style={styles.follow} onClick={uploadHandler(user.id,this.props.loggedInUser)}>
                <Entypo name="plus" size={20} color = {colors.text} />
                <Text style={styles.followText}>Follow</Text>
              </TouchableOpacity>):(null)}
            </View>
          </View>
        </LinearGradient>
    )

  }
}

const uploadHandler = (user, loggedIn) => {
  let formdata = new FormData();

  formdata.append("followerId", user);
  console.log(formdata);

  var settings = {
    method: "PUT",
    body: formdata,
  };
  console.log(settings);
  fetch("http://34.141.19.16:8083/users/"+loggedIn+"/followers", settings);
  console.log(settings.body);


};

const styles = StyleSheet.create({
  imageContainer: {
    // ...gs.center, dunno why is not working like this, should do the same like next two lines
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
  },
  check: {
    // ...gs.center,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.text,
    borderRadius: 15,
    width: 32,
    height: 32,
    shadowColor: colors.darkBg,
    shadowOffset: { height: 3, width: 1 },
    shadowOpacity: 0.3,
    position: "absolute",
    zIndex: 1,
    right: -7,
    bottom: 10,
  },
  follow: {
    // ...gs.button,  // does not apply yet
    justifyContent: "center",
    backgroundColor: colors.green,
    borderRadius: 100,
    // ...gs.rowCenter,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 17,
    paddingVertical: 8,
    marginTop: 16,
    borderColor: "#ffffff",
    borderWidth: 1.7,
  },
  followText: {
    fontSize: 16,
    color: colors.text,
    fontWeight: "600",
    marginLeft: 4,
  },
});
