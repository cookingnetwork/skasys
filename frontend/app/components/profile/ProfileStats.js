import React, {useEffect, useState} from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { gs, colors } from "../../config/styles"
import {useFocusEffect} from "@react-navigation/native";

export default function ProfileStats(props) {

  const [USERS, setUsers] = useState([]);
  const [Num, setNum] = useState(0);

  useFocusEffect(
      React.useCallback(() => {
        fetch('http://34.141.19.16:8083/users/' + props.user.id + '/followers')
            .then((response) => response.json())
            .then((json) => setUsers(json))
            .catch((error) => console.error(error));

        fetch('http://34.141.19.16:8083/users/' + props.user.id + '/following/num')
            .then((response) => response.json())
            .then((json) => setNum(json))
            .catch((error) => console.error(error));
      }, []));

  useEffect(() => {
    fetch('http://34.141.19.16:8083/users/' + props.user.id + '/followers')
        .then((response) => response.json())
        .then((json) => setUsers(json))
        .catch((error) => console.error(error));

    fetch('http://34.141.19.16:8083/users/' + props.user.id + '/following/num')
        .then((response) => response.json())
        .then((json) => setNum(json))
        .catch((error) => console.error(error));
  }, []);

  return (
      <View style={styles.container}>

        <View style={styles.statContainer}>
          <Text style={styles.statNumber}>{Num}</Text>
          <Text style={styles.stat}>Followers</Text>
        </View>

        <View style={[styles.statContainer, styles.divider]}>
          <Text style={styles.statNumber}>{USERS.length}</Text>
          <Text style={styles.stat}>Following</Text>
        </View>

        <View style={styles.statContainer}>
          <Text style={styles.statNumber}>{props.postAmount}</Text>
          <Text style={styles.stat}>Posts</Text>
        </View>

      </View>
  )
}

const styles = StyleSheet.create({
  container: {
    // ...gs.sectionContainer,
    paddingVertical: 20,
    paddingHorizontal: 27,
    marginBottom: 8,
    marginTop: -40,
    backgroundColor: colors.lightBg,
    // ...gs.rowBetween,
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 16,
    borderRadius: 16,
  },
  statContainer: {
    // ...gs.center,
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  statNumber: {
    fontSize: 20,
    fontWeight: "600",
    color: colors.text,
  },
  stat: {
    fontSize: 11,
    fontWeight: "600",
    letterSpacing: 1,
    textTransform: "uppercase",
    color: colors.lightHl,
    marginTop: 4
  },
  divider: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: colors.darkHl,
  }

})
