import React from "react";
import styled from "styled-components/native";
import {View, ImageBackground, TouchableOpacity, Text} from "react-native";
import { AntDesign, HeartOutlined } from "@expo/vector-icons";

import StyledText from "./StyledText";
import{ colors} from "../config/styles";

function PostImage({ data, navigation, user }) {
  return (
    <FoodPicture
      source={"http://34.141.19.16:8083/posts/" + data.id + "/image"}
    >
      <HeartView>
        <AntDesign
          name="heart"
          size={27}
          color={colors.red}
          style={{
            justifyContent: "flex-end",
            alignItems: "flex-end",
            position: "absolute",
          }}
        />
      </HeartView>
        <PlusView>
            <TouchableOpacity onPress={() => navigation.navigate("CreateMeet", { id: data.id, user: user.id, postId: data.postId })} style={styles.btn}>
                <Text style={{color:colors.white}}>Meeting erstellen</Text>
            </TouchableOpacity>
        </PlusView>
    </FoodPicture>
  );
}

export default PostImage;

const styles = {
    loginBtn: {
        width: "30%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        backgroundColor: colors.darkBg,
    },
    btn: {
        width: "20%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        backgroundColor: colors.darkBg,
    }
}

const FoodPicture = styled.ImageBackground`
  width: 100%;
  height: 350px;
`;

const RecipeTitle = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  flex: 1;
  padding: 10px;
`;

const PlusView = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  flex: 1;
  padding: 10px;
`;

const HeartView = styled.View`
  justify-content: flex-start;
  align-items: flex-end;
  margin: 10px;
`;
