import React , { useState, useEffect} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native';
import TestHomeScreen from '../screens/PostScreen'
import FeedScreen from '../screens/FeedScreen'
import ProfileScreen from '../screens/ProfileScreen';
import PostScreen from '../screens/PostScreen'
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import { gs, colors } from "../config/styles"
import { Ionicons, Entypo, FontAwesome5 } from '@expo/vector-icons'
import HotFeedScreen from "../screens/HotFeedScreen";
import CreatePostScreen from "../screens/CreatePostScreen";
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';
import MeetFeedScreen from "../screens/MeetFeedScreen";
import MyMeetsScreen from "../screens/MyMeetsScreen";
import CreateMeetScreen from "../screens/CreateMeetScreen";

const Tab = createBottomTabNavigator()

const CustomTabBarButton = ({ children, onPress }) => (
    <TouchableOpacity
        style={{
            top: -30,
            justifyContent: 'center',
            alignItems: 'center',
            ...styles.shadow
        }}
        onPress={onPress}>
        <View style={{
            width: 70,
            height: 70,
            borderRadius: 35,
            backgroundColor: colors.red,
        }}>
            {children}
        </View>
    </TouchableOpacity>
)

const CustomIncognito = ({ children, onPress }) => (
    <TouchableOpacity
        style={{
            top: -30,
            justifyContent: 'center',
            alignItems: 'center',
            ...styles.shadow
        }}
        onPress={onPress}>
        <View style={{
            width: 1,
            height: 1,
            borderRadius: 1,
            backgroundColor: colors.white,
        }}>
            {children}
        </View>
    </TouchableOpacity>
)

const NavMenue = (props) => {
    const [isLoggedIn, setLogin] = useState(false);
    const [user, setUser] = useState(0);
    useEffect(() => {
    });
    return (
        <Tab.Navigator
            tabBarOptions={{
                showLabel: false,
                //
                style: {
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    right: 0,
                    elevation: 0,
                    backgroundColor: colors.white,
                    borderRadius: 0,
                    height: 70,
                    // ...styles.shadow
                }
            }}>
            {!isLoggedIn ? (
                <>
                <Tab.Screen name="Login"
                            options={{
                                tabBarIcon: ({focused}) => (
                                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                        <FontAwesome5 name="sign-in-alt" size={20} color={colors.darkHl}/>
                                        {/* <Image
                source={require('../assets/icon.png')}
                resizeMode='contain'
                style={{
                  width: 25, height: 25, tintColor: focused ? '#e32f45' : '748c94'
                }} /> */}
                                        <Text
                                            style={{
                                                color: focused ? colors.white : colors.darkBg, fontSize: 12
                                            }}>Login</Text>
                                    </View>
                                ),
                            }}>{props => <LoginScreen {...props} setLoginA={setLogin} setUserA={setUser}/>}</Tab.Screen>
                    <Tab.Screen name="Register" component={RegisterScreen}
                                options={{
                                    tabBarButton: (props) => (
                                        <CustomIncognito {...props} />
                                    )
                                }} />
                    <Tab.Screen name="ForgotPassword" component={ForgotPasswordScreen}
                                options={{
                                    tabBarButton: (props) => (
                                        <CustomIncognito {...props} />
                                    )
                                }} />
                </>
            ) : (
                <>
                    <Tab.Screen name="Profile" 
                                initialParams={{userId:user}}
                                options={{
                                    tabBarIcon: ({ focused }) => (
                                        <View style={{ alignItems: 'center', justifyContent: 'center'}}>
                                            <FontAwesome5 name="user-circle" size={20} color = {colors.darkHl} />
                                            <Text
                                                style={{
                                                    color: focused ? colors.white : colors.darkBg, fontSize: 12
                                                }}>Profile</Text>
                                        </View>
                                    ),
                                }}
                    >{props => <ProfileScreen {...props} userId={user}/>}</Tab.Screen>
                    <Tab.Screen name="CreateMeet" component={CreateMeetScreen} initialParams={{user:user}}
                                options={{
                                    tabBarButton: (props) => (
                                        <CustomIncognito {...props} />
                                    )
                                }} />
                    <Tab.Screen name="ProfileCook"
                                options={{
                                    tabBarButton: (props) => (
                                        <CustomIncognito {...props} />
                                    )
                                }}
                     >{props => <ProfileScreen {...props} userId={user}/>}</Tab.Screen>
                    <Tab.Screen name="Meets" component={MeetFeedScreen} initialParams={{userId:user}}
                                options={{
                                    tabBarIcon: ({ focused }) => (
                                        <View style={{ alignItems: 'center', justifyContent: 'center'}}>
                                            <FontAwesome5 name="concierge-bell" size={20} color = {colors.darkHl} />
                                            <Text
                                                style={{
                                                    color: focused ? colors.white : colors.darkBg, fontSize: 12
                                                }}>Meets</Text>
                                        </View>
                                    ),
                                }}
                />
            <Tab.Screen name="Create" component={CreatePostScreen} initialParams={{user:user}}
                        options={{
                            tabBarIcon: ({ focused }) => (
                                <FontAwesome5 name="plus" size={35} color = {colors.white} />
                            ),
                            tabBarButton: (props) => (
                                <CustomTabBarButton {...props} />
                            )
                        }} />
                    <Tab.Screen name="Post" component={PostScreen}
                                options={{
                                    tabBarButton: (props) => (
                                        <CustomIncognito {...props} />
                                    )
                                }} />

            <Tab.Screen name="Feed" component={FeedScreen} initialParams={{user:user}}
                        options={{
                            tabBarIcon: ({ focused }) => (
                                <View style={{ alignItems: 'center', justifyContent: 'center'}}>
                                    <FontAwesome5 name="delicious" size={20} color = {colors.darkHl} />
                                    <Text
                                        style={{
                                            color: focused ? colors.white : colors.darkBg, fontSize: 12
                                        }}>Feed</Text>
                                </View>
                            ),
                        }}/>
                    <Tab.Screen name="Meet" component={MyMeetsScreen}
                                options={{
                                    tabBarButton: (props) => (
                                        <CustomIncognito {...props} />
                                    )
                                }} />
            <Tab.Screen name="HotFeed" component={HotFeedScreen}
                        options={{
                            tabBarIcon: ({ focused }) => (
                                <View style={{ alignItems: 'center', justifyContent: 'center'}}>
                                    <FontAwesome5 name="burn" size={20} color = {colors.darkHl} />
                                    <Text
                                        style={{
                                            color: focused ? colors.white : colors.darkBg, fontSize: 12
                                        }}>HotFeed</Text>
                                </View>
                            ),
                        }}
            />
                </>) }
        </Tab.Navigator>
    )
}

export default NavMenue

const styles = StyleSheet.create({
    shadow: {
        shadowColor: '7F5DF0',
        shadowOffset: {
            width: 0,
            height: 10
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.5,
        elevation: 5
    }
})
