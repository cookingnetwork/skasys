import React, { useEffect, useState } from "react";

import styled from "styled-components/native";
import { View, ImageBackground, Image, TouchableOpacity, } from "react-native";

import StyledText from "./StyledText";
import {colors} from "../config/styles";

function PostCook( {user,navigation}) {
  const Cook = {
    userId: 123,
    name: "Sang Woo Bae",
    image: require("../assets/stockface.jpeg"),
  };

  const { name, image } = Cook;
  
  return (
    <CookBar>
      <TouchableOpacity>
        <ProfilePicture source={image} onClick={() => navigation.navigate("ProfileCook", { userId: user.id })}/>
      </TouchableOpacity>
      <TouchableOpacity>
        <StyledText large light heavy onClick={() => navigation.navigate("ProfileCook", { userId: user.id })}>
          {user.firstName} {user.name}
        </StyledText>
      </TouchableOpacity>
      
    </CookBar>
  );
}

export default PostCook;

const CookBar = styled.View`
  background-color: ${colors.darkBg};
  height: 70px;
  align-items: center;
  justify-content: flex-start;
  flex-direction: row;
`;

const ProfilePicture = styled.Image`
  width: 60px;
  height: 60px;
  border-radius: 30px;
  border-width: 2px;
  border-color: ${colors.green};
  margin-left: 5px;
  margin-right: 5px;
`;

