import React, { useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    SafeAreaView,
    Platform,
    StatusBar,
    ImageBackground,
    FlatList,
    ScrollView,
} from "react-native";
import styled from "styled-components/native";
import { AntDesign } from "@expo/vector-icons";

import StyledText from "./StyledText";
import colors from "../config/colors";

function MyMeets({ data }) {
    const ing = data.participants;
    return (
        <PreparationContainer>
            <PreparationView>
            <StyledText large light heavy>
                Wo:
            </StyledText>
            <StyledText medium light>{data.location + "\n"}</StyledText>
            </PreparationView>
            <SeparationLine/>
            <PreparationView>
                <StyledText large light heavy>
                    Wann:
                </StyledText>
                <StyledText medium light>{data.date + "\n"}</StyledText>
            </PreparationView>
            <SeparationLine/>
            <PreparationView>
                <StyledText large light heavy>
                    Informationen:
                </StyledText>
                <StyledText medium light>{data.description}</StyledText>
            </PreparationView>
            <SeparationLine/>
            <Ingredients>
                <StyledText large light heavy>
                    Teilnehmer:
                </StyledText>

            </Ingredients>

        </PreparationContainer>
    );
}
export default MyMeets;
/*<View style={{ flexDirection: "column" }}>
                    {ing.map((item) => {
                        return (
                            <View key={Math.random().toString()}>
                                <StyledText light>{item + "\n"}</StyledText>
                            </View>
                        );
                    })}
                </View>*/
const PreparationContainer = styled.View`
  flex-direction: column;
  margin-top: 20px;
  margin-bottom: 20px;
  margin-left: 100px;
  margin-right:100px;
  align-items:'center';
`;

const Ingredients = styled.View`
  margin-top: 10px;
  margin-bottom: 10px;
`;

const PreparationView = styled.View`
  margin-top: 10px;
  margin-bottom: 10px;
`;

const Title = styled.View`
  font-size: 1.5em;
  color: colors.white;
`;

const SeparationLine = styled.View`
  border-bottom-color: ${colors.green};
  border-bottom-width: 2px;
`;
