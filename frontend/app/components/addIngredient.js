import React, { useState } from "react";
import { Stylesheet, Text, View, TextInput, Button } from "react-native";
import {colors} from "../config/styles";

export default function AddIngredient({ submitHandler }) {
  const [name, setName] = useState("");

  const changeHandler = (val) => {
    setName(val);
  };

  return (
    <View>
      <TextInput placeholder="Neue Zutat..." onChangeText={changeHandler} />
      <Button color={colors.lightBg} onPress={() => submitHandler(name)} title="add ingredient" />
    </View>
  );
}
