import { StyleSheet } from "react-native";
import { color } from "react-native-reanimated";

export const colors = {
  darkBg: "#222",
  lightBg: "#333",
  darkHl: "#666",
  lightHl: "#888",
  red: "#f20c05",
  green: "#47b44d",
  white: "#ffffff",
  red_1:'#e32f45',

  text: "#fff",
  textSec: "#aaa",
};

export const gs = StyleSheet.create({
  sectionContainer: {
    paddingVertical: 24,
    paddingHorizontal: 32,
    marginBottom: 8,
    backgroundColor: colors.darkBg
  },
  sectionTitle: {
    fontWeight: "700",
    color: colors.text,
    fontSize: 15,
  },
  rowCenter: {
    flexDirection: "row",
    alignItems: "center"
  },
  rowBetween: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  divider: {
    borderBottomColor: "#444",
    borderBottomWidth: 1,
    marginVertical: 24,
  },
  title: {
    color: colors.text,
    fontSize: 30,
  },
  subTitle: {
    fontWeight: "600",
    textTransform: "uppercase",
    color: color.text,
    fontSize: 15,
    letterSpacing: 1,
  },
  smallText: {
    fontSize: 12,
    fontWeight: "800",
    color: colors.text,
  },
  absoluteFull: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.red,
    borderRadius: 100,
  },
});
