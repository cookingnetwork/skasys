import React from 'react';
import { View,  Text, Button } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';

const TestHomeScreen = ({navigation}) => {
  return (
    <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
      <Text> Home Screen</Text>
      <Button
      title ="Go to FeedScreen"
      onPress={() => navigation.navigate("FeedScreen")}
      />
    </View>
  )
  
}

 export default TestHomeScreen