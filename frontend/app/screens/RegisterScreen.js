import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect} from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";
import {colors} from "../config/styles";
 
const RegisterScreen = ({ navigation }) =>  {
  const [name, setName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const loginUser = () => {
    register(name, firstName, email, password)
      .then(() => {
        navigation.navigate('Login');
      })
      .catch((err) => console.log('error:', err.message));
  };
 
  return (
    <View style={styles.container}>

        <Text>Registration</Text>
        <Text>Do not use your real credentials here!</Text>

       {/* Image loading not Working? */}
      <Image style={styles.image} source={require("../assets/brunch.jpg")} />

      <StatusBar style="auto" />
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Surname"
          placeholderTextColor="#fff"
          onChangeText={(name) => setName(name)}
        />
      </View>

      <StatusBar style="auto" />
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="First Name"
          placeholderTextColor="#fff"
          onChangeText={(firstName) => setFirstName(firstName)}
        />
      </View>

      <StatusBar style="auto" />
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Email"
          placeholderTextColor="#fff"
          onChangeText={(email) => setEmail(email)}
        />
      </View>
 
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Password"
          placeholderTextColor="#fff"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>
 
      <TouchableOpacity>
        <Text onPress={() => navigation.navigate('ForgotPassword')} style={styles.forgot_button}>Forgot Password?</Text>
      </TouchableOpacity>
 
      <TouchableOpacity onPress={() => loginUser()} style={styles.loginBtn}>
        <Text style={styles.loginText}>REGISTER</Text>
      </TouchableOpacity>
    </View>
  );
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
 
  image: {
    marginBottom: 40,
  },
 
  inputView: {
    backgroundColor: colors.lightBg,
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
 
    alignItems: "center",
  },
 
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    color: '#fff'
  },
 
  forgot_button: {
    height: 30,
    marginBottom: 30,
  },
 
  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: colors.green,
  },
  loginText: {
    color: "#fff"
  }
});




  //CHANGE HERE FOR REAL METHOD CALLS TO LOGIN SERVICE!!!!!
  export const register = (name, firstName, email, password, shouldSucceed = true) => {
    console.log(name, firstName, email, password);
    var settings = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: name,
        firstName: firstName,
        email: email,
        password: password,
      }),
    };

    return fetch('http://34.141.19.16:8083/account/register',settings);
  };

  export const createAccount = (email, password, shouldSucceed = true) => {
    console.log(email, password);
  
    if (!shouldSucceed) {
      return mockFailure({ error: 500, message: 'Something went wrong!' });
    }
  
    return mockSuccess({ auth_token: 'successful_fake_token' });
  };
export default RegisterScreen;