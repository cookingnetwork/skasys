import React, { useEffect, useState } from "react";
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    StatusBar,
    Image,
    Button, TouchableOpacity,
} from "react-native";
import PostScreen from "./PostScreen";
import App from "../../App";
import {colors} from "../config/styles";
import {LinearGradient} from "expo-linear-gradient";
import { useFocusEffect } from '@react-navigation/native';

const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Scallion Pancakes',
        date: '30.11.2020',
        uri: require('../assets/scallion.jpg')
    },
    ];

let Item = ({ title, date, uri, participants, navigation, id, adresse, userId }) => (
    <View style={styles.item}>
        <LinearGradient colors={[colors.green, colors.red]} start={[0, 3]} end={[1, 2]} style={{borderRadius: 30}}>
            <View style={styles.item}>
                <Image style={styles.image} source={uri} />
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.infos}>{date}</Text>
                <TouchableOpacity onPress={() => navigation.navigate("Meet", { id: id, userId: userId })} style={styles.loginBtn}>
                    <Text style={{color:colors.white}}>Mehr anzeigen</Text>
                </TouchableOpacity>
            </View>
        </LinearGradient>
    </View>
);

const MeetFeedScreen = ({ navigation, route }) => {
    const [DATA, setData] = useState([]);
    const [USERS, setUsers] = useState([]);

    useFocusEffect(
        React.useCallback(() => {
            fetch('http://34.141.19.16:8083/meetings')
                .then((response) => response.json())
                .then((json) => setData(json))
                .catch((error) => console.error(error));
        }, []));

    useEffect(() => {
        fetch('http://34.141.19.16:8083/meetings')
            .then((response) => response.json())
            .then((json) => setData(json))
            .catch((error) => console.error(error));
    }, []);


    const renderItem = ({ item }) => (
        <Item title={item.name} uri={"http://34.141.19.16:8083/posts/"+item.postId+"/image"} date={item.date} navigation={navigation} id={item.id} adresse={item.location} userId={route.params.userId}/>
    );

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={renderItem}
                keyExtractor={(item) => item.id.toString()}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        marginBottom: 70,
        backgroundColor: colors.darkBg
    },
    item: {
        padding: 20,
        marginVertical: 10,
        marginHorizontal: 120,
        alignItems: 'center',
    },
    user: {
        fontSize: 20,
    },
    image: {
        width: 500,
        height: 300,
    },
    title: {
        fontSize: 30,
        color: colors.darkBg,
        marginTop: 12
    },
    loginBtn: {
        width: "30%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        backgroundColor: colors.lightBg,
    },
    infos:{
        fontSize: 20,
        color: colors.darkBg,
        marginTop: 12
    }
});

export default MeetFeedScreen;
