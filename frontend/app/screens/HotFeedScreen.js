import React, { useEffect, useState } from "react";
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    StatusBar,
    Image,
    Button, TouchableOpacity,
} from "react-native";
import PostScreen from "./PostScreen";
import App from "../../App";
import {colors} from "../config/styles";
import {LinearGradient} from "expo-linear-gradient";
import {FontAwesome5} from "@expo/vector-icons";
import styled from "styled-components/native/dist/styled-components.native.esm";
import { useFocusEffect } from '@react-navigation/native';

/*const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Scallion Pancakes',
        owner: 'Michael J.',
        uri: require('../assets/scallion.jpg')
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Erdbeerkuchen',
        owner: 'Susanne H.',
        uri: require('../assets/erdbeer.jpg')
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Kokosnuss Hähnchen',
        owner: 'LeckaSchmecka51',
        uri: require('../assets/curry.jpg')
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d62',
        title: 'Brunch Ideen',
        owner: 'RobertKocht',
        uri: require('../assets/brunch.jpg')
    },
];*/


let Item = ({ title, uri, owner, navigation, id }) => (
    <View style={styles.item}>
        <LinearGradient colors={[colors.green, colors.red]} start={[0, 3]} end={[1, 2]} style={{borderRadius: 30}}>
            <View style={styles.item}>
    <Image style={styles.image} source={uri} />
    <Text style={styles.title}>{title}</Text>
    <TouchableOpacity onPress={() => navigation.navigate("Post", { id: id })} style={styles.loginBtn}>
                    <Text style={{color:colors.white}}>Mehr erfahren</Text>
                </TouchableOpacity>
  </View>
        </LinearGradient>
    </View>
);

const HotFeedScreen = ({ navigation }) => {
  const [DATA, setData] = useState([]);
  const [USERS, setUsers] = useState([]);


  useFocusEffect(
    React.useCallback(() => {
      fetch('http://34.141.19.16:8083/feed/hot')
            .then((response) => response.json())
            .then((json) => setData(json))
            .catch((error) => console.error(error));
    }, []));

    useEffect(() => {
        fetch('http://34.141.19.16:8083/feed/hot')
            .then((response) => response.json())
            .then((json) => setData(json))
            .catch((error) => console.error(error));
    }, []);


    const renderItem = ({ item }) => (
        <Item title={item.name} uri={"http://34.141.19.16:8083/posts/"+item.id+"/image"} owner={item.userId} navigation={navigation} id={item.id}/>
    );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        marginBottom: 70,
        backgroundColor: colors.darkBg
    },
    item: {
        padding: 20,
        marginVertical: 10,
        marginHorizontal: 120,
        alignItems: 'center',
    },
    user: {
        fontSize: 20,
    },
    image: {
        width: 500,
        height: 300,
    },
    title: {
        fontSize: 30,
        color: colors.darkBg,
        marginTop: 12
    },
    loginBtn: {
        width: "30%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        backgroundColor: colors.lightBg,
    },
});

const Title = styled.View`
  font-size: 1.5em;
  color: colors.darkBg;
  margin-top: 12px;
`;
const Textsmall = styled.View`
  font-size: 0.5em;
  color: colors.darkBg;
  margin-bottom: 12px;
`;
const Textbutton = styled.View`
  font-size: 0.5em;
  color: colors.white;
`;

const ButtonNeu = styled.Button`
  /* Adapt the colors based on primary prop */
  color: colors.darkBg;

  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid colors.darkBg;
  border-radius: 3px;
`;


export default HotFeedScreen;
