import React, {useEffect, useState} from 'react';
import {SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, Image, Button, TouchableOpacity} from 'react-native';
import {gs, colors} from "../config/styles";
import { LinearGradient } from 'expo-linear-gradient'
import { useFocusEffect } from '@react-navigation/native';

/*const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Scallion Pancakes',
        owner: 'Michael J.',
        uri: require('../assets/scallion.jpg')
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Erdbeerkuchen',
        owner: 'Susanne H.',
        uri: require('../assets/erdbeer.jpg')
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Kokosnuss Hähnchen',
        owner: 'LeckaSchmecka51',
        uri: require('../assets/curry.jpg')
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d62',
        title: 'Brunch Ideen',
        owner: 'RobertKocht',
        uri: require('../assets/brunch.jpg')
    },
];*/

const Item = ({ title, uri, owner, navigation, id }) => (

        <View style={styles.item}>
            <LinearGradient colors={[colors.green, colors.red]} start={[0, 3]} end={[1, 2]} style={{borderRadius: 30}}>
                <View style={styles.item}>
                <Image style= {styles.image} source={uri}/>
                    <Text style={styles.title}>{title}</Text>
                    <TouchableOpacity onPress={() => navigation.navigate("Post", { id: id })} style={styles.loginBtn}>
                        <Text style={{color:colors.white}}>Mehr erfahren</Text>
                    </TouchableOpacity>
                </View>
            </LinearGradient>
        </View>


);

const FeedScreen = (props) => {
    console.log(props);
    const [DATA, setData] = useState([]);

    useFocusEffect(
        React.useCallback(() => {
            fetch('http://34.141.19.16:8083/feed/follower/'+ props.route.params.user)
                .then((response) => response.json())
                .then((json) => setData(json))
                .catch((error) => console.error(error));
        }, []));

    useEffect(() => {
        fetch('http://34.141.19.16:8083/feed/follower/'+ props.route.params.user)
            .then((response) => response.json())
            .then((json) => setData(json))
            .catch((error) => console.error(error));
    }, []);

    const renderItem = ({ item }) => (
        <Item title={item.name} uri={"http://34.141.19.16:8083/posts/"+item.id+"/image"} owner={item.userId} navigation={props.navigation} id={item.id}/>
    );

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        marginBottom: 70,
        backgroundColor: colors.darkBg
    },
    item: {
        padding: 20,
        marginVertical: 10,
        marginHorizontal: 120,
        alignItems: 'center',
    },
    user: {
        fontSize: 20,
    },
    image: {
        width: 500,
        height: 300,
    },
    title: {
        fontSize: 30,
        color: colors.darkBg,
        marginTop: 12
    },
    loginBtn: {
        width: "30%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        backgroundColor: colors.lightBg,
    },
});

export default FeedScreen;