import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";
import NavMenue from "../components/NavMenue";
import {colors} from "../config/styles";
import { useFocusEffect } from '@react-navigation/native';

const ForgotPasswordScreen = ({ navigation}) =>  {
  const [email, setEmail] = useState("");

  const sendRequest = () => {
    forgotPassword(email);
    navigation.navigate("Login")
  };

  return (
      <View style={styles.container}>

        <Text>Password Recovery</Text>
        <Text>Type in your account email. If the account exists, the password will be send to the email. </Text>

        {/* Image loading not Working? */}
        <Image style={styles.image} source={require("../assets/brunch.jpg")} />

        <StatusBar style="auto" />
        <View style={styles.inputView}>
          <TextInput
              style={styles.TextInput}
              placeholder="Email"
              placeholderTextColor="#fff"
              onChangeText={(email) => setEmail(email)}
          />
        </View>

        <TouchableOpacity onPress={() => sendRequest()} style={styles.loginBtn}>
          <Text style={styles.loginText}>SEND EMAIL</Text>
        </TouchableOpacity>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    marginBottom: 40,
  },

  inputView: {
    backgroundColor: colors.lightBg,
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,

    alignItems: "center",
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    color: '#fff'
  },

  forgot_button: {
    height: 30,
    marginBottom: 30,
  },

  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: colors.green,
  },

  loginText: {
    color: "#fff"
  }
});



//CHANGE HERE FOR REAL METHOD CALLS TO LOGIN SERVICE!!!!!
export const forgotPassword = (email) => {

  var data = new FormData()
  data.append('email', email)

  var settings = {
    method: "POST",
    body:data
  };

  return fetch('http://34.141.19.16:8083/account/forgot',settings)

};

export default ForgotPasswordScreen;