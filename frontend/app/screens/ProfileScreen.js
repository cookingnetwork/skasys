import React from 'react'
import {
  ScrollView,
  View,
  StyleSheet,
  StatusBar
} from 'react-native'
import { gs, colors } from '../config/styles'
import ProfileHeader from '../components/profile/ProfileHeader'
import ProfileStats from '../components/profile/ProfileStats'
import ProfileContent from '../components/profile/ProfileContent'

export default class ProfileScreen extends React.Component {
  state = {
    user: {},
    isLoading: true,
    posts: [],
    meets: []
  }

  
  async componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', async () => {
      try {
        var id = this.props.route.params.userId
        let userRes = await fetch("http://34.141.19.16:8083/users/"+id)
        let user = await userRes.json()

        let meetsRes = await fetch("http://34.141.19.16:8083/meetings/user/"+id)
        let meets = await meetsRes.json()

        let postsRes = await fetch("http://34.141.19.16:8083/posts/user/"+id)
        let posts = await postsRes.json()
  
        this.setState({ user: user })
        this.setState({ isLoading: false })
        this.setState({ posts: posts})
        this.setState({ meets: meets})
      } catch (err) {
        console.log(err)
      }
    });
  }

 
  componentWillUnmount() {
    this._unsubscribe();
  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={[gs.center, styles.container]}>
          <StatusBar barStyle="light-content" />
        </View>
      )
    }

    return (
      <ScrollView style={styles.container}>
        <ProfileHeader user={this.state.user} loggedInUser={this.props.userId}/>
        <ProfileStats user={this.state.user} postAmount={this.state.posts.length}/>
        <ProfileContent user={this.state.user} posts={this.state.posts} navigation={this.props.navigation} meets={this.state.meets}/>
      </ScrollView>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.darkBg,
  },
})
