import React, { useState, useEffect } from "react";
import {
  Text,
  TextInput,
  View,
  SafeAreaView,
  Platform,
  StatusBar,
  FlatList,
  Alert,
  Button,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
} from "react-native";
import { Buffer } from "buffer";

import * as ImagePicker from "expo-image-picker";
import styled from "styled-components/native";

import StyledText from "../components/StyledText.js";
import TopBar from "../components/TopBar.js";
import { colors } from "../config/styles";
import Ingredient from "../components/Ingredient.js";
import AddIngredient from "../components/addIngredient.js";

function CreateMeetScreen(props) {
  const [title, setTitle] = useState("");
  const [date, setDate] = useState("");
  const [location, setLocation] = useState("");
  const [description, setDescription] = useState("");
  const [participants, setParticipants] = useState("");

  useEffect(() => {}, []);

  const uploadHandler = () => {
    var maxParticipants = parseInt(participants);

    var settings = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        postId: props.route.params.id,
        creatorId: props.route.params.user,
        name: title,
        date: date,
        location: location,
        description: description,
        maxParticipants: maxParticipants,
      }),
    };
    fetch("http://34.141.19.16:8083/meetings", settings);
    console.log(settings.body);

  };

  const changeTitleHandler = (val) => {
    setTitle(val);
  };

  const changeDateHandler = (val) => {
    setDate(val);
  };

  const changeLocationHandler = (val) => {
    setLocation(val);
  };

  const changeDescriptionHandler = (val) => {
    setDescription(val);
  };

  const changeParticipantsHandler = (val) => {
    setParticipants(val);
  };

  return (
    <TouchableWithoutFeedback
      vertical
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <Container
        style={{
          paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        }}
      >
        <View style={{ paddingBottom: 200 }}>
          <TopBar data={{ name: "New Meeting" }} />
          <SeparationLine />
          {/* Bild vom Rezept */}
          <FoodPicture
            source={
              "http://34.141.19.16:8083/posts/" +
              props.route.params.id +
              "/image"
            }
          />
          <SeparationLine />
          {/* Meetingtitel */}
          <Title dark large>
            Name des Meetings:
          </Title>
          <TitleInput
            onChangeText={changeTitleHandler}
            placeholder="Meetingnamen eingeben"
          />
          {/* Datum */}
          <Separation />
          <Title dark large>
            Datum des Meetings:
          </Title>
          <TitleInput
            onChangeText={changeDateHandler}
            placeholder="Datum eingeben"
          />
          {/* Ort */}
          <Separation />
          <Title dark large>
            Ort des Meetings:
          </Title>
          <TitleInput
            onChangeText={changeLocationHandler}
            placeholder="Ort eingeben"
          />
          {/* Anzahl der Teilnehmer */}
          <Separation />
          <Title dark large>
            Teilnehmeranzahl:
          </Title>
          <TitleInput
            onChangeText={changeParticipantsHandler}
            placeholder="Teilnehmer angeben"
          />
          {/* Beschreibung */}
          <Separation />
          <Title dark large>
            Beschreibung:
          </Title>

          <PreparationInput
            onChangeText={changeDescriptionHandler}
            placeholder="Beschreibung eingeben"
            multiline={true}
            numberOfLines={10}
          />

          <UploadButton
            color={colors.lightBg}
            title="Hochladen"
            onPress={() => uploadHandler()}
          />
        </View>
      </Container>
    </TouchableWithoutFeedback>
  );
}

export default CreateMeetScreen;

const Container = styled.ScrollView`
  flex: 1;
  background-color: ${colors.white};
`;

const SeparationLine = styled.View`
  border-bottom-color: ${colors.darkBg};
  border-bottom-width: 2px;
`;

const Separation = styled.View`
  border-bottom-color: ${colors.white};
  border-bottom-width: 10px;
`;

const TitleInput = styled.TextInput`
  height: 40px;
  margin: 12px;
  border-width: 1px;
`;

const PreparationInput = styled.TextInput`
  margin: 12px;
  border-width: 1px;
  margin-bottom: 20px;
`;

const FoodPicture = styled.Image`
  width: 100%;
  height: 350px;
`;

const IngredientList = styled.View`
  margin: 10px;
`;

const UploadButton = styled.Button`
  margin-bottom: 400px;
`;

const Title = styled.View`
  font-size: 1.5em;
  color: ${colors.darkBg};
  margin: 12px;
`;
