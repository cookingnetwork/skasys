import React, { useState, useEffect } from "react";
import {
  Text,
  TextInput,
  View,
  SafeAreaView,
  Platform,
  StatusBar,
  FlatList,
  Alert,
  Button,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
} from "react-native";
import { Buffer } from "buffer";

import * as ImagePicker from "expo-image-picker";
import styled from "styled-components/native";

import StyledText from "../components/StyledText.js";
import TopBar from "../components/TopBar.js";
import { colors } from "../config/styles";
import Ingredient from "../components/Ingredient.js";
import AddIngredient from "../components/addIngredient.js";

function CreatePostScreen(props) {
  const [title, setTitle] = useState("");
  const [ingredients, setIngredients] = useState([]);
  const [preparation, setPreparation] = useState("");
  const [image, setImage] = useState(require("../assets/PlatzhalterBild.png"));

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  {
    /* For deleting Ingredients */
  }
  const deleteIngredientHandler = (key) => {
    setIngredients((prevIngredients) => {
      return prevIngredients.filter((ingredient) => ingredient.key != key);
    });
  };

  const changeTitleHandler = (val) => {
    setTitle(val);
  };

  const uploadHandler = () => {
    console.log("Upload");
    const ingred = ingredients.map((ingredient) => ingredient.name);
    let your_bytes = Buffer.from(image, "base64");
    console.log(image);
    console.log(your_bytes);
    var array = Array.from(your_bytes.slice(15));
    var settings = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId: props.route.params.user,
        name: title,
        ingredients: ingred,
        description: preparation,
        image: array,
      }),
    };
    fetch("http://34.141.19.16:8083/posts", settings);
    console.log(settings.body);

    let formdata = new FormData();

    formdata.append("file", image);
    formdata.append("type", "image/jpeg");
    formdata.append("name", "name");
    console.log(formdata);
    /*fetch("http://34.141.29.130:8083/posts/7/image", {
      method: "post",
      body: formdata,
    });*/
  };

  const changePreparationHandler = (val) => {
    setPreparation(val);
  };

  {
    /* For adding Ingredients */
  }
  const submitHandler = (name) => {
    if (name.length > 0) {
      setIngredients((prevIngredients) => {
        return [
          { name: name, key: Math.random().toString() },
          ...prevIngredients,
        ];
      });
    } else {
      Alert.alert("Bitte Zutat eingeben");
    }
  };

  return (
    <TouchableWithoutFeedback
      vertical
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <Container
        style={{
          paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        }}
      >
        <View style={{ paddingBottom: 200 }}>
          <TopBar data={{ name: "New Post" }} />
          <SeparationLine />
          {/* Bild vom Rezept */}
          <FoodPicture source={image} />
          <Button
            title="Choose Photo"
            color={colors.lightBg}
            onPress={() => pickImage()}
          />

          {/* Rezepttitel */}
          <Title dark large>
            Rezepttitel:
          </Title>
          <TitleInput
            onChangeText={changeTitleHandler}
            placeholder="Rezepttitel eingeben"
          />
          {/* Zutaten */}
          <Separation />
          <Title dark large>
            Zutaten:
          </Title>
          <AddIngredient submitHandler={submitHandler} />
          <IngredientList>
            <FlatList
              vertical
              data={ingredients}
              renderItem={({ item }) => (
                <Ingredient
                  item={item}
                  deleteIngredientHandler={deleteIngredientHandler}
                />
              )}
            />
          </IngredientList>
          {/* Zubereitung */}
          <Title dark large>
            Zubereitung:
          </Title>

          <PreparationInput
            onChangeText={changePreparationHandler}
            placeholder="Zubereitung eingeben"
            multiline={true}
            numberOfLines={10}
          />
          <UploadButton
            color={colors.lightBg}
            title="Hochladen"
            onPress={() => uploadHandler()}
          />
        </View>
      </Container>
    </TouchableWithoutFeedback>
  );
}

export default CreatePostScreen;

const Container = styled.ScrollView`
  flex: 1;
  background-color: ${colors.white};
`;

const SeparationLine = styled.View`
  border-bottom-color: ${colors.darkBg};
  border-bottom-width: 2px;
`;

const Separation = styled.View`
  border-bottom-color: ${colors.white};
  border-bottom-width: 10px;
`;

const TitleInput = styled.TextInput`
  height: 40px;
  margin: 12px;
  border-width: 1px;
`;

const PreparationInput = styled.TextInput`
  margin: 12px;
  border-width: 1px;
  margin-bottom: 20px;
`;

const FoodPicture = styled.Image`
  width: 100%;
  height: 350px;
`;

const IngredientList = styled.View`
  margin: 10px;
`;

const UploadButton = styled.Button`
  margin-bottom: 400px;
`;

const Title = styled.View`
  font-size: 1.5em;
  color: ${colors.darkBg};
  margin: 12px;
`;
