import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity, ScrollView,
} from "react-native";
import NavMenue from "../components/NavMenue";
import {colors} from "../config/styles";
import { useFocusEffect } from '@react-navigation/native';

const LoginScreen = ({ navigation, route, setLoginA, setUserA}) =>  {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoggedIn, setLoginResponse] = useState(false);
  const [userId, setUserId] = useState(0);


  useEffect(() => {
    setLoginA(isLoggedIn);
    setUserA(userId);
    //if(isLoggedIn) navigation.navigate('HotFeed')
  });

  const loginUser = () => {
    login(email, password)
        .then((response) => response.json())
        .then(function(json){
          setUserId(json.userID);
          setLoginResponse(json.loggedIn);
        })
        .catch((error) => console.error(error));
  };

  return (
      <View style={styles.container}>

        <Text>Login</Text>
        <Text>Do not use your real credentials here!</Text>

        {/* Image loading not Working? */}
        <Image style={styles.image} source={require("../assets/brunch.jpg")} />

        <StatusBar style="auto" />
        <View style={styles.inputView}>
          <TextInput
              style={styles.TextInput}
              placeholder="Email"
              placeholderTextColor="#fff"
              onChangeText={(email) => setEmail(email)}
          />
        </View>

        <View style={styles.inputView}>
          <TextInput
              style={styles.TextInput}
              placeholder="Password"
              placeholderTextColor="#fff"
              secureTextEntry={true}
              onChangeText={(password) => setPassword(password)}
          />
        </View>

        <TouchableOpacity>
          <Text onPress={() => navigation.navigate('ForgotPassword')} style={styles.forgot_button}>Forgot Password?</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text onPress={() => navigation.navigate('Register')} style={styles.forgot_button}>New here? Create an Account now!</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => loginUser()} style={styles.loginBtn}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    marginBottom: 40,
  },

  inputView: {
    backgroundColor: colors.lightBg,
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
    alignItems: "center",
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    color: '#fff',
    
  },

  forgot_button: {
    height: 30,
    marginBottom: 30,
  },

  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: colors.green,
  },

  loginText: {
    color: "#fff"
  }
});



//CHANGE HERE FOR REAL METHOD CALLS TO LOGIN SERVICE!!!!!
export const login = (email, password, shouldSucceed = true) => {
  var settings = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email,
      password: password,
    }),
  };

  return fetch('http://34.141.19.16:8083/account/login',settings)

};

export const createAccount = (email, password, shouldSucceed = true) => {
  if (!shouldSucceed) {
    return mockFailure({ error: 500, message: 'Something went wrong!' });
  }

  return mockSuccess({ userId: '1234' });
};
export default LoginScreen;