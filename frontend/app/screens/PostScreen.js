import React, { useEffect, useState } from "react";
import {
  View,
  SafeAreaView,
  Platform,
  StatusBar,
  ScrollView,
} from "react-native";

import styled from "styled-components/native";
import { AntDesign } from "@expo/vector-icons";

import StyledText from "../components/StyledText.js";
import TopBar from "../components/TopBar.js";
import {colors} from "../config/styles";
import PostImage from "../components/PostImage.js";
import PostCook from "../components/PostCook.js";
import Preparation from "../components/Preparation.js";
import {LinearGradient} from "expo-linear-gradient";
import ProfileHeader from "../components/profile/ProfileHeader";

function PostScreen(props) {
  var [Data, setData] = useState({
    id: 1,
    userId: 0,
    name: "",
    description: "",
    ingredients: [""],
    image: null,
  });
  var [isLoading, setLoading] = useState(true);
  const [user, setUser] = useState([]); 


  useEffect(() => {
    async function fetchData() {
      const response = await fetch("http://34.141.19.16:8083/posts/" + props.route.params.id);
      const json = await response.json();
      setData(json)
  
      const response2 = await fetch('http://34.141.19.16:8083/users/'+json.userId);
      const json2 = await response2.json();
      setUser(json2)
    }
    fetchData();
  }, [props.route.params.id]);

  return (
    <Container>
      <View style={{}}>
        <TopBar data={Data}/>
        <PostImage data={Data} navigation={props.navigation} user={user}/>
        <SeparationLine />
        <PostCook user={user} navigation={props.navigation}/>
        <Preparation data={Data} />
      </View>
    </Container>
  );
}

export default PostScreen;

const Container = styled.ScrollView`
  flex: 1;
  background-color: ${colors.lightBg};
  padding-top: ${Platform.OS === "android" ? StatusBar.currentHeight : 0}px;
  padding-bottom: 500px;
`;

const SeparationLine = styled.View`
  border-bottom-color: ${colors.green};
  border-bottom-width: 2px;
`;
