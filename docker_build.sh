#!/bin/bash
lnk = "eu.gcr.io/spice-up-318715"

cd api-gateway
ag-pushn = "${lnk}/api-gateway"
docker build -t .
docker tag api-gateway ag-pushn
docker push ag-pushn
cd ..

cd feed-service
docker build -t .
docker tag feed-service eu.gcr.io/spice-up-318715/feed-service
docker push eu.gcr.io/spice-up-318715/feed-service
cd ..

cd user-service
docker build -t .
docker tag user-service eu.gcr.io/spice-up-318715/user-service
docker push eu.gcr.io/spice-up-318715/user-service
cd ..

cd post-service
docker build -t .
docker tag post-service eu.gcr.io/spice-up-318715/post-service
docker push eu.gcr.io/spice-up-318715/post-service
cd ..
