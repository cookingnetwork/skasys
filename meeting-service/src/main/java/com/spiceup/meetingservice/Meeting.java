package com.spiceup.meetingservice;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Meeting {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Long creatorId;
    private Long postId;
    private Integer maxParticipants;
    @ElementCollection
    private List<Long> participants;
    private String date;
    private String location;
    private String description;
}
