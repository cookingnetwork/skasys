package com.spiceup.meetingservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MeetingService {

    private MeetingRepository meetingRepository;
    private WebClient.Builder webClientBuilder;

    @Autowired
    public MeetingService(MeetingRepository meetingRepository, WebClient.Builder webClientBuilder) {
        this.meetingRepository = meetingRepository;
        this.webClientBuilder = webClientBuilder;
    }

    public List<Meeting> getAllMeetings() {
        List<Meeting> meetings = new ArrayList<>();
        meetingRepository.findAll().forEach(meetings::add);
        return meetings;
    }

    public String createMeeting(Meeting meeting) {
        Meeting createdMeeting = meetingRepository.save(meeting);
        List<Long> participants = new ArrayList<>();
        participants.add(meeting.getCreatorId());
        createdMeeting.setParticipants(participants);
        meetingRepository.save(createdMeeting);
        return "Created meeting with id "+createdMeeting.getId();
    }

    public List<Meeting> getMeetingsByUserId(Long creatorId) {
        return meetingRepository.findByCreatorId(creatorId);
    }


    public Meeting getMeetingById(Long id) {
        Optional<Meeting> meetingOptional = meetingRepository.findById(id);
        if (meetingOptional.isEmpty()) return null;

        return meetingOptional.get();
    }

    public String addUserAsParticipant(Long meetingId, Long userId) {
        Optional<Meeting> meetingOptional = meetingRepository.findById(meetingId);
        if (meetingOptional.isEmpty()) return "Could not find meeting with id "+meetingId;

        Meeting meeting = meetingOptional.get();
        List<Long> participants = meeting.getParticipants();
        if(meeting.getMaxParticipants() == participants.size()){
            return "Could not add participant. Maximal amount of participants was reached!";
        }
        else if(participants.contains(userId)){
            return "Could not add participant, because the user already participates!";
        }
        participants.add(userId);
        meeting.setParticipants(participants);
        meetingRepository.save(meeting);
        return "Added participant with id "+userId;

    }

    public String removeUserAsParticipant(Long meetingId, Long userId) {
        Optional<Meeting> meetingOptional = meetingRepository.findById(meetingId);
        if (meetingOptional.isEmpty()) return "Could not find meeting with id "+meetingId;

        Meeting meeting = meetingOptional.get();
        List<Long> participants = meeting.getParticipants();
        if(!participants.contains(userId)){
            return "Could not remove participant, because the user did not participate!";
        }
        else if(userId.equals(meeting.getCreatorId())){
            return "Could not remove participant, because the user is the creator!";
        }
        participants.remove(userId);
        meeting.setParticipants(participants);
        meetingRepository.save(meeting);
        return "Removed participant with id "+userId;
    }
}
