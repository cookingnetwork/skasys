package com.spiceup.meetingservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/meetings")
public class MeetingController {

    private MeetingService meetingService;

    @Autowired
    public MeetingController(MeetingService meetingService) {
        this.meetingService = meetingService;
    }

    @GetMapping
    public List<Meeting> getAllMeetings(){
        return meetingService.getAllMeetings();
    }

    @GetMapping("/{id}")
    public Meeting getMeetingById(@PathVariable Long id){
        return meetingService.getMeetingById(id);
    }

    @PostMapping
    public String createMeeting(@RequestBody Meeting meeting){
        return  meetingService.createMeeting(meeting);
    }

    @GetMapping("/user/{userId}")
    public List<Meeting> getMeetingsByUserID(@PathVariable Long userId){
        return meetingService.getMeetingsByUserId(userId);
    }

    @PutMapping("/{id}/participant/{userId}")
    public String addUserAsParticipant(@PathVariable Long id, @PathVariable Long userId){
        return meetingService.addUserAsParticipant(id,userId);
    }

    @DeleteMapping("/{id}/participant/{userId}")
    public String removeUserAsParticipant(@PathVariable Long id, @PathVariable Long userId){
        return meetingService.removeUserAsParticipant(id,userId);
    }
}
