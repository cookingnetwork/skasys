package com.spiceup.userservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUsers() {
        return StreamSupport
                .stream(userRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Long createUser(User user) {
        return userRepository.save(user).getId();
    }

    public boolean addPostToUser(Long userId, Long postId) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.getPostsById().add(postId);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    public List<User> getAllFollowers(Long userId) {
        Optional<User> currentUser = userRepository.findById(userId);
        if (currentUser.isEmpty()) return Collections.emptyList();
        final List<Long> followersByIds = currentUser.get().getFollowersByUserId();
        return followersByIds.stream()
                .map(id -> {
                    Optional<User> user = findUserById(id);
                    return user.orElse(null);
                })
                .collect(Collectors.toList());
    }

    public boolean addFollower(Long userId, Long followerId) {
        Optional<User> userById = findUserById(userId);
        if (userById.isEmpty()) {
            log.warn("user with userID " + userId + " can't be found");
            return false;
        }
        User user = userById.get();
        if (user.getFollowersByUserId().stream().noneMatch(l -> (l.equals(followerId)))) {
            user.getFollowersByUserId().add(followerId);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    public Optional<User> findUserById(Long userId) {
        return userRepository.findById(userId);
    }

    public boolean removeFollower(Long userId, Long followerId) {
        Optional<User> userById = findUserById(userId);
        if (userById.isEmpty()) {
            log.warn("user with userID " + userId + " can't be found");
            return false;
        }
        User user = userById.get();
        user.getFollowersByUserId().remove(followerId);
        userRepository.save(user);
        return true;
    }

    public boolean addFollowing(Long userId, Long following) {
        Optional<User> userById = findUserById(userId);
        if (userById.isEmpty()) {
            log.warn("user with userID " + userId + " can't be found");
            return false;
        }
        User user = userById.get();
        if (user.getFollowingByUserId().stream().noneMatch(l -> (l.equals(following)))) {
            user.getFollowingByUserId().add(following);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    public boolean removeFollowing(Long userId, Long following) {
        Optional<User> userById = findUserById(userId);
        if (userById.isEmpty()) {
            log.warn("user with userID " + userId + " can't be found");
            return false;
        }
        User user = userById.get();
        user.getFollowingByUserId().remove(following);
        userRepository.save(user);
        return true;
    }

    public Long numOfFollowing(Long userId) {
        Optional<User> userById = findUserById(userId);
        if (userById.isEmpty()) {
            log.warn("user with userID " + userId + " can't be found");
            return 0L;
        }
        User user = userById.get();
        return (long) user.getFollowingByUserId().size();
    }

    public boolean addLikedPost(Long userId, Long postId) {
        Optional<User> userById = findUserById(userId);
        if (userById.isEmpty()) {
            log.warn("user with user Id " + userId + " can't be found.");
            return false;
        }
        User user = userById.get();
        user.getLikedPostsById().add(postId);
        userRepository.save(user);
        return true;
    }

    public boolean removeLikedPost(Long userId, Long postId) {
        Optional<User> userById = findUserById(userId);
        if (userById.isEmpty()) {
            log.warn("user with user Id " + userId + " can't be found.");
            return false;
        }
        User user = userById.get();
        user.getLikedPostsById().remove(postId);
        userRepository.save(user);
        return true;
    }
}
