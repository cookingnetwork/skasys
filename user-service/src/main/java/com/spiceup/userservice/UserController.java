package com.spiceup.userservice;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("/create")
    public Long createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PutMapping("/{userId}/{postId}")
    public boolean addPostToUser(@PathVariable Long userId, @PathVariable Long postId) {
        return userService.addPostToUser(userId, postId);
    }

    @GetMapping("{userId}")
    public User getUserById(@PathVariable Long userId) {
        return userService.findUserById(userId).orElse(null);
    }

    @GetMapping("{userId}/followers")
    public List<User> getAllFollowers(@PathVariable Long userId) {
        return userService.getAllFollowers(userId);
    }

    @PutMapping("{userId}/followers")
    public boolean addFollowersToUser(@PathVariable Long userId, @RequestParam("followerId") Long followerId) {
        return userService.addFollower(userId, followerId);
    }

    @DeleteMapping("{userId}/followers")
    public boolean deleteFollowersToUser(@PathVariable Long userId, @RequestParam("followerId") Long followerId) {
        return userService.removeFollower(userId, followerId);
    }

    @PutMapping("likes/{userId}/{postId}")
    public boolean addLikedPost(@PathVariable Long userId, @PathVariable Long postId) {
        return userService.addLikedPost(userId, postId);
    }

    @GetMapping("/{userId}/following/num")
    public Long getNumOfFollowing(@PathVariable Long userId) {
        return userService.numOfFollowing(userId);
    }

    @PutMapping("/{userId}/following")
    public boolean addFollowingToUser(@PathVariable Long userId, @RequestParam("followingId") Long followingId) {
        return userService.addFollowing(userId, followingId);
    }

    @DeleteMapping("/{userId}/following")
    public boolean deleteFollowingFromUser(@PathVariable Long userId, @RequestParam("followingId") Long followingId) {
        return userService.removeFollowing(userId, followingId);
    }

    @DeleteMapping("likes/{userId}/{postId}")
    public boolean removeLikedPost(@PathVariable Long userId, @PathVariable Long postId) {
        return userService.removeLikedPost(userId, postId);
    }
}
