package com.spiceup.userservice;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
class User {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String firstName;
    @ElementCollection
    private List<Long> postsById;
    @ElementCollection
    private List<Long> followersByUserId;
    @ElementCollection
    private List<Long> followingByUserId;
    @ElementCollection
    private List<Long> likedPostsById;
}
