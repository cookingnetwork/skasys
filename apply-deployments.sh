#!/bin/bash
echo "Applying all deployments to cluster..."
kubectl apply -f post-service/deployment-pv.yaml
kubectl apply -f post-service/deployment-database.yaml

kubectl apply -f user-service/deployment-pv.yaml
kubectl apply -f user-service/deployment-database.yaml

kubectl apply -f account-service/deployment-pv.yaml
kubectl apply -f account-service//deployment-database.yaml

kubectl apply -f meeting-service/deployment-pv.yaml
kubectl apply -f meeting-service//deployment-database.yaml

kubectl apply -f feed-service/deployment.yaml

#make sure database are already running
sleep 40

kubectl apply -f post-service/deployment.yaml

kubectl apply -f user-service/deployment.yaml

kubectl apply -f api-gateway/deployment.yaml

kubectl apply -f account-service/deployment.yaml

kubectl apply -f meeting-service/deployment.yaml