#!/bin/bash

cd api-gateway
docker build -t api-gateway .
docker tag api-gateway eu.gcr.io/spice-up-318715/api-gateway
docker push eu.gcr.io/spice-up-318715/api-gateway
cd ..

cd feed-service
docker build -t feed-service .
docker tag feed-service eu.gcr.io/spice-up-318715/feed-service
docker push eu.gcr.io/spice-up-318715/feed-service
cd ..

cd user-service
docker build -t user-service .
docker tag user-service eu.gcr.io/spice-up-318715/user-service
docker push eu.gcr.io/spice-up-318715/user-service
cd ..

cd post-service
docker build -t post-service .
docker tag post-service eu.gcr.io/spice-up-318715/post-service
docker push eu.gcr.io/spice-up-318715/post-service
cd ..

cd account-service
docker build -t account-service .
docker tag account-service eu.gcr.io/spice-up-318715/account-service
docker push eu.gcr.io/spice-up-318715/account-service
cd ..

cd meeting-service
docker build -t meeting-service .
docker tag meeting-service eu.gcr.io/spice-up-318715/meeting-service
docker push eu.gcr.io/spice-up-318715/meeting-service
cd ..