package com.spiceup.postservice;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostRepository extends CrudRepository<Post,Long> {

    List<Post> findByUserId(Long userId);

    List<Post> findByName(String name);
}
