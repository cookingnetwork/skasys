package com.spiceup.postservice;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="post")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long userId;
    private String name;
    private String description;
    private Long likes = 0L;
    @ElementCollection
    private List<String> ingredients;
    @ElementCollection
    private List<Long> likedByUserById;
    @Lob
    private Byte[] image;

}
