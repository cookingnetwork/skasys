package com.spiceup.postservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * This class implements a REST Controller for the Post Service's API.
 * It implements no logic and acts solely as a controller.
 */
@RestController
@RequestMapping(value = "/posts")
public class PostController {

    final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping
    public String addPost(@RequestBody Post post) {
        return postService.addPost(post);
    }

    @PutMapping("/{id}")
    public String editPostById(@RequestBody Post post, @PathVariable Long id) {
        return postService.editPostById(post, id);
    }

    @DeleteMapping("/{id}")
    public String deletePostById(@PathVariable Long id) {
        return postService.deletePostById(id);
    }

    @GetMapping("/{id}")
    public Post getPostById(@PathVariable Long id) {
        return postService.getPostById(id);
    }

    @GetMapping("/user/{id}")
    public List<Post> getPostsByUserId(@PathVariable Long id) {
        return postService.getPostsByUserId(id);
    }

    @GetMapping("/search/{name}")
    public List<Post> getPostsByName(@PathVariable String name) {
        return postService.getPostsByName(name);
    }

    @GetMapping()
    public List<Post> getAllPosts() {
        return postService.getAllPosts();
    }

    @PostMapping("/{id}/image")
    public String handleImagePost(@PathVariable Long id, @RequestParam("file") MultipartFile file) {
        return postService.saveImageFile(id, file);
    }

    @GetMapping(path = "/{id}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getImageById(@PathVariable Long id) {
        return postService.getImageFile(id);
    }

    @GetMapping("/likes/{postId}")
    public Long numOfLikes(@PathVariable Long postId) {
        return postService.getNumOfLikes(postId);
    }

    @PutMapping("/likes/{postId}/{userId}")
    public Post addLike(@PathVariable Long postId, @PathVariable Long userId) {
        return postService.addUserLikes(postId, userId);
    }

    @DeleteMapping("/likes/{postId}/{userId}")
    public Post deleteLike(@PathVariable Long postId, @PathVariable Long userId) {
        return postService.removeUserLike(postId, userId);
    }
}
