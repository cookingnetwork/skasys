package com.spiceup.postservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class implements the logic of the Post Service. It is called directly form the Post Controller.
 */

@Slf4j
@Service
public class PostService {

    private final PostRepository postRepository;
    private final PostProperties postProperties;
    private final WebClient.Builder webClientBuilder;

    @Autowired
    public PostService(PostRepository postRepository, PostProperties postProperties, WebClient.Builder webClientBuilder) {
        this.postRepository = postRepository;
        this.postProperties = postProperties;
        this.webClientBuilder = webClientBuilder;
    }

    public String addPost(Post post) {
        Post createdPost = postRepository.save(post);
        addPostToUser(createdPost);
        return "Added post successfully";
    }

    public String editPostById(Post post, Long id) {
        post.setId(id);
        postRepository.save(post);
        return "Edited post with id " + id + " successfully.";
    }

    public String deletePostById(Long id) {
        postRepository.deleteById(id);
        return "Deleted post with id " + id;
    }

    public List<Post> getPostsByUserId(Long userId) {
        List<Post> posts = new ArrayList<>();
        postRepository.findByUserId(userId).forEach(post -> {
            post.setImage(null);
            posts.add(post);
        });
        return posts;
    }

    public List<Post> getPostsByName(String name) {
        List<Post> posts = new ArrayList<>();
        postRepository.findByName(name).forEach(post -> {
            post.setImage(null);
            posts.add(post);
        });
        return posts;
    }

    public List<Post> getAllPosts() {
        List<Post> posts = new ArrayList<>();
        postRepository.findAll().forEach(post -> {
                    post.setImage(null);
                    posts.add(post);
                }
        );
        return posts;
    }

    public String saveImageFile(Long id, MultipartFile file) {
        try {
            Optional<Post> postOptional = postRepository.findById(id);
            if (postOptional.isEmpty()) return "Could not save Image. There is no post with id " + id + ".";

            Post post = postOptional.get();
            Byte[] byteObjects = new Byte[file.getBytes().length];
            int i = 0;
            for (byte b : file.getBytes()) {
                byteObjects[i++] = b;
            }

            post.setImage(byteObjects);
            postRepository.save(post);
            return "Saved image on post " + id;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Saving image failed on post " + id;
    }

    public byte[] getImageFile(Long id) {
        Optional<Post> postOptional = postRepository.findById(id);
        if (postOptional.isEmpty()) return null;

        Post post = postOptional.get();
        if (post.getImage() == null) return null;
        byte[] imageConverted = new byte[post.getImage().length];
        int j = 0;
        for (Byte b : post.getImage())
            imageConverted[j++] = b;
        return imageConverted;

    }

    public Post getPostById(Long id) {
        Optional<Post> postOptional = postRepository.findById(id);
        if (postOptional.isEmpty()) return null;

        return postOptional.get();
    }

    public void addPostToUser(Post post) {
        //make call to user-service to add postId to User
        String uri = "http://" + postProperties.getUserServiceServer() + ":" + postProperties.getUserServicePort() + "/users/" + post.getUserId() + "/" + post.getId();
        String isAdded = webClientBuilder.build()
                .put()
                .uri(uri)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public Long getNumOfLikes(Long postId) {
        Optional<Post> postById = postRepository.findById(postId);
        if (postById.isEmpty()) {
            log.warn("Post " + postId + " can't be found");
            return null;
        }
        Post post = postById.get();
        return post.getLikes();
    }

    public Post addUserLikes(Long postId, Long userId) {
        Optional<Post> postById = postRepository.findById(postId);
        if (postById.isEmpty()) {
            log.warn("Post " + postId + " can't be found");
            return null;
        }
        Post post = postById.get();
        if (!isPostLikedByUser(postId, userId) && !userId.equals(post.getUserId())) {
            post.getLikedByUserById().add(userId);
            post.setLikes(post.getLikes() + 1);
            addLikedPostToUser(postId, userId);
            postRepository.save(post);
        }
        return post;
    }

    public Post removeUserLike(Long postId, Long userId) {
        Optional<Post> postById = postRepository.findById(postId);
        if (postById.isEmpty()) {
            log.warn("Post " + postId + " can't be found");
            return null;
        }
        Post post = postById.get();
        if (post.getLikedByUserById().remove(userId)) {
            post.setLikes(post.getLikes() - 1);
            removeLikedPostFromUser(postId, userId);
        }
        return post;
    }

    private boolean addLikedPostToUser(Long postId, Long userId) {
        Optional<Post> postById = postRepository.findById(postId);
        if (postById.isEmpty()) {
            log.warn("Post " + postId + " can't be found");
            return false;
        }
        final String uri = "http://" + postProperties.getUserServiceServer() + ":" + postProperties.getUserServicePort() + "/users/likes/" + userId + "/" + postId;
        String isAdded = webClientBuilder.build()
                .put()
                .uri(uri)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        return isAdded != null;
    }

    private boolean removeLikedPostFromUser(Long postId, Long userId) {
        Optional<Post> postById = postRepository.findById(postId);
        if (postById.isEmpty()) {
            log.warn("Post " + postId + " can't be found");
            return false;
        }
        final String uri = "http://" + postProperties.getUserServiceServer() + ":" + postProperties.getUserServicePort() + "/users/likes/" + userId + "/" + postId;
        String isRemoved = webClientBuilder.build()
                .delete()
                .uri(uri)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        return isRemoved != null;
    }

    private boolean isPostLikedByUser(Long postId, Long userId) {
        Optional<Post> postById = postRepository.findById(postId);
        if (postById.isEmpty()) {
            log.warn("Post " + postId + " can't be found");
            return false;
        }
        Post post = postById.get();
        return post.getLikedByUserById().stream().anyMatch(userId::equals);
    }
}
