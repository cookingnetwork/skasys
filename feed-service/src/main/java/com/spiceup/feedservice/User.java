package com.spiceup.feedservice;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.util.List;

@Getter
@Setter
@NoArgsConstructor
class User {
    private Long id;
    private String name;
    private String firstName;
    private List<Long> postsById;
    private List<Long> followersByUserId;
}
