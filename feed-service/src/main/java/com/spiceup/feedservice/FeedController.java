package com.spiceup.feedservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/feed")
public class FeedController {

    private FeedService feedService;

    @Autowired
    public FeedController (FeedService feedService) {
        this.feedService = feedService;
    }

    @GetMapping("/hot")
    public List<Post> getHotFeed(){
        return feedService.getHotFeed();
    }

    @GetMapping("/follower/{userId}")
    public List<Post> getFollowerFeedByUserId(@PathVariable Long userId){
        return feedService.getFollowerFeed(userId);
    }

}
