package com.spiceup.feedservice;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Post {
    private Long id;
    private Long userId;
    private String name;
    private String description;
    private List<String> ingredients;
    private Byte[] image;
}
