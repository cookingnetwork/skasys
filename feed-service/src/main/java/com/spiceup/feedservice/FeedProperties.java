package com.spiceup.feedservice;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "spiceup")
@Data
public class FeedProperties {
    private String postServiceServer;
    private String postServicePort;
    private String userServiceServer;
    private String userServicePort;
}
