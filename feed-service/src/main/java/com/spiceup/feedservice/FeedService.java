package com.spiceup.feedservice;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Getter
public class FeedService {

    private FeedProperties feedProperties;
    private WebClient.Builder webClientBuilder;

    @Autowired
    public FeedService(FeedProperties feedProperties, WebClient.Builder webClientBuilder) {
        this.feedProperties = feedProperties;
        this.webClientBuilder = webClientBuilder;
    }

    public List<Post> getHotFeed() {
        //request to post-service
        String uri = "http://"+feedProperties.getPostServiceServer()+":"+feedProperties.getPostServicePort()+"/posts";
        List<Post> posts = webClientBuilder.build()
                .get()
                .uri(uri)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Post>>() {})
                .block();
        //TODO: (possible) future implementation: filter by likes, etc?
        if (posts != null) {
            Collections.shuffle(posts);
        }
        return posts;
    }

    public List<Post> getFollowerFeed(Long userId) {
        //TODO: (authorize?)
        List<Long> followedUsers = getFollowedUsersByUserId(userId);
        List<Post> followerFeed = new ArrayList<>();
        for (Long followedUser : followedUsers){
            String uri = "http://"+feedProperties.getPostServiceServer()+":"+feedProperties.getPostServicePort()+"/posts/user/"+followedUser;
            List<Post> posts = webClientBuilder.build()
                    .get()
                    .uri(uri)
                    .retrieve()
                    .bodyToMono(new ParameterizedTypeReference<List<Post>>() {})
                    .block();
            if (posts != null) {
                followerFeed.addAll(posts);
            }
        }
        //TODO: (possible) future implementation: maybe randomize order, order by date
        return followerFeed;
    }

    private List<Long> getFollowedUsersByUserId(Long userId){
        //TODO: Make call to user-service
        String uri = "http://"+feedProperties.getUserServiceServer()+":"+feedProperties.getUserServicePort()+"/users/"+userId+"/followers";
        List<User> followedUsers = webClientBuilder.build()
                .get()
                .uri(uri)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<User>>() {})
                .block();
        if (followedUsers != null) {
            return followedUsers.stream().map(User::getId).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
